#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>
#include <math.h>

using namespace std;
using namespace cv;

//prosedur konversi ke grayscale
void grayscaleImage(Mat source, Mat result)
{
    int grayValue, b, g, r;

    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            b = source.at<Vec3b>(i,j)[0];
            g = source.at<Vec3b>(i,j)[1];
            r = source.at<Vec3b>(i,j)[2];

            grayValue = (b+g+r)/3;

            result.at<Vec3b>(i,j)[0] = grayValue;
            result.at<Vec3b>(i,j)[1] = grayValue;
            result.at<Vec3b>(i,j)[2] = grayValue;
        }
    }
}

//prosedur konversi ke gray negative
void negativeImageGray(Mat source, Mat result)
{

    int grayValue;

    grayscaleImage(source,result);

    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            grayValue = result.at<Vec3b>(i,j)[0];

            result.at<Vec3b>(i,j)[0] = 255 - grayValue;
            result.at<Vec3b>(i,j)[1] = 255 - grayValue;
            result.at<Vec3b>(i,j)[2] = 255 - grayValue;
        }
    }
}

//prosedur konversi ke color negative
void negativeImageColor(Mat source, Mat result)
{

    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            result.at<Vec3b>(i,j)[0] = 255 - source.at<Vec3b>(i,j)[0];
            result.at<Vec3b>(i,j)[1] = 255 - source.at<Vec3b>(i,j)[1];
            result.at<Vec3b>(i,j)[2] = 255 - source.at<Vec3b>(i,j)[2];
        }
    }
}

//prosedur contrast stretching untuk citra grayscale
void contrastStretchingGray(Mat source, Mat result)
{

    int r1 = 255;
    int r2 = 0;

    grayscaleImage(source,result);

    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            if (result.at<Vec3b>(i,j)[0] > r2)
            {
                r2 = result.at<Vec3b>(i,j)[0];
            }

            if (result.at<Vec3b>(i,j)[0] < r1)
            {
                r1 = result.at<Vec3b>(i,j)[0];
            }
        }
    }

    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            result.at<Vec3b>(i,j)[0] = (result.at<Vec3b>(i,j)[0] - r1)*((255-0)/(r2-r1));
            result.at<Vec3b>(i,j)[1] = (result.at<Vec3b>(i,j)[1] - r1)*((255-0)/(r2-r1));
            result.at<Vec3b>(i,j)[2] = (result.at<Vec3b>(i,j)[2] - r1)*((255-0)/(r2-r1));
        }
    }
}

//prosedur contrast stretching untuk citra berwarna
void contrastStretchingColor(Mat source, Mat result)
{
    //inisialisasi variabel
    float r1 = 255.0;
    float r2 = 0.0;

    float R,G,B,I,Rx,Gx,Bx;

    //mencari intensitas maksimum dan minimum
    for (int i=0; i<source.rows; i++)
    {
        for (int j=0; j<source.cols; j++)
        {
            R = source.at<Vec3b>(i,j)[2];
            G = source.at<Vec3b>(i,j)[1];
            B = source.at<Vec3b>(i,j)[0];

            I = round((R + G + B)/3.0);

            if (I > r2)
            {
                r2 = I;
            }

            if (I < r1)
            {
                r1 = I;
            }
        }
    }

    //proses transformasi contrast stretching dan proses redraw citra
    for(int i=0; i<source.rows; i++)
    {
        for(int j=0; j<source.cols; j++)
        {
            Rx = source.at<Vec3b>(i,j)[2];
            Gx = source.at<Vec3b>(i,j)[1];
            Bx = source.at<Vec3b>(i,j)[0];

            Rx = round(((Rx - r1)/(r2-r1))*255.0);
            Gx = round(((Gx - r1)/(r2-r1))*255.0);
            Bx = round(((Bx - r1)/(r2-r1))*255.0);

            result.at<Vec3b>(i,j)[2] = (int)Rx;
            result.at<Vec3b>(i,j)[1] = (int)Gx;
            result.at<Vec3b>(i,j)[0] = (int)Bx;
        }
    }
}

//program utama
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cout<<"Penggunaan : ./Tugas1PCD <-g/-c> <nama_file_citra>"<<endl;
        return -1;
    }

    Mat imageOriginal = imread(argv[2],1);

    Mat imageGrayscale = Mat::zeros(imageOriginal.size(),imageOriginal.type());
    Mat imageNegative = Mat::zeros(imageOriginal.size(),imageOriginal.type());
    Mat imageStretched = Mat::zeros(imageOriginal.size(),imageOriginal.type());

    Mat imageProc = imageOriginal.clone();

    if ((string)argv[1] == "-g")
    {
        negativeImageGray(imageProc,imageNegative);
        contrastStretchingGray(imageProc,imageStretched);
    }
    else if ((string)argv[1] == "-c")
    {
        negativeImageColor(imageProc,imageNegative);
        contrastStretchingColor(imageProc,imageStretched);
    }

    namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    imshow("Original Image", imageOriginal);

    namedWindow("Image Negatives", CV_WINDOW_AUTOSIZE);
    imshow("Image Negatives", imageNegative);
    string imageName = "negative_" + (string) argv[2];
    imwrite(imageName, imageNegative);

    namedWindow("Contrast Stretched Image", CV_WINDOW_AUTOSIZE);
    imshow("Contrast Stretched Image", imageStretched);
    string imageName2 = "stretched_" + (string) argv[2];
    imwrite(imageName2, imageStretched);

    waitKey(0);
    imageOriginal.release();
    imageGrayscale.release();
    imageNegative.release();
    imageStretched.release();
    return 0;
}
